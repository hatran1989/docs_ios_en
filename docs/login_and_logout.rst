.. GTVSDKDoc documentation master file, created by
   sphinx-quickstart on Thu Mar 14 15:46:07 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Login & logout
=====================================

Get UserInfo
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- Step 1: After login, register or relaunch the game (logged in before), you can get user hash::

	NSString *userHash = [GTVManager getUserHash];

- Step 2: using userHash to call API get user info in Server side docs


**Note: We will send private Server side docs to you**

Login & logout
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

	#import <GTVCocoaTouchFramework/GTVCocoaTouchFramework.h>

- Add GTVManagerDelegate to your class::

	@interface YourClass ()<GTVManagerDelegate>

	@end

- Show login::

	GTVManager.sharedManager.delegate = self;

    	[GTVManager.sharedManager showLogin];

- Handle GTVManagerDelegate::

	- (void)gtvDidReceivedUserHash:(NSString *)userHash {

	}

	- (void)gtvDidClickLoginFacebook {
		[[GTVManager sharedManager] loginFacebookWith:yourViewController];
	}

	- (void)gtvDidClickPlayNow {

	}

	- (void)gtvDidLogout {

	}

